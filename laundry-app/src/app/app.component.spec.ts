import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { RoomsMenuComponent } from './components/rooms-menu/rooms-menu.component';
import { MachinesMenuComponent } from './components/machines-menu/machines-menu.component';
import { MachineCardComponent } from './components/machine-card/machine-card.component';
import { MachineListComponent } from './components/machine-list/machine-list.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StateToIconPipe } from './pipes/state-to-icon.pipe';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule
      ],
      declarations: [
        AppComponent,
        HeaderComponent,
        RoomsMenuComponent,
        MachinesMenuComponent,
        MachineCardComponent,
        MachineListComponent,
        StateToIconPipe
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
