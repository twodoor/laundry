import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { LAUNDRY_ROOMS } from '../mock/json.mock';
import { of, BehaviorSubject, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { Room, RoomI } from '../models/room';

@Injectable({
  providedIn: 'root'
})
export class RoomsService {
  private rooms$ = new BehaviorSubject<Room[]>([]);

  constructor() {
    this.loadRooms();
  }

  loadRooms() {
    return of(LAUNDRY_ROOMS).pipe(
      map(roomObjects => {
        const rooms = roomObjects.map((roomObject: RoomI) => {
          return new Room(roomObject);
        });
        this.rooms$.next(rooms);
      })
    ).subscribe();

    // return this.http.get<RoomI[]>(environment.apiUrl + '/rooms').subscribe(result => {
    //   console.log(result);
    // });
  }

  getLaundryRoom(id: number): Observable<Room> {
    return this.rooms$.pipe(
      map((rooms: Room[]) => {
        return rooms.find((room: Room) => room.id === id);
      })
    );
  }

  getRooms(): Observable<Room[]> {
    return this.rooms$;
  }
}
