import { Injectable } from '@angular/core';
import { Machine, MachineI } from '../models/machine';
import { WASHING_MACHINES } from '../mock/json.mock';
import { BehaviorSubject, of, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MachinesService {
  private machines$ = new BehaviorSubject<Machine[]>([]);

  constructor() {
    this.loadWashingMachines();
  }

  loadWashingMachines() {
    return of(WASHING_MACHINES).pipe(
      map(machineObjects => {
        const machines = machineObjects.map((machineObject: any) => {
          return new Machine(machineObject);
        });
        this.machines$.next(machines);
      })
    ).subscribe();

    // return this.http.get<MachineI[]>(environment.apiUrl + '/machines').subscribe(result => {
    // update cache
    // });
  }

  updateMachine(id: number, updatedMachine: Machine) {
    // return this.http.patch<MachineI>(environment.apiUrl + `/machines/${id}`).subscribe(result => {
    // update cache
    const indexOfMachineToUpdate = this.machines$.getValue().findIndex(machine => machine.id === id);
    this.machines$.getValue()[indexOfMachineToUpdate] = updatedMachine;
    this.machines$.next(this.machines$.getValue());
    // });
  }

  createMachine(machine: Machine) {
    // return this.http.post<MachineI>(environment.apiUrl + `/machines`).subscribe(result => {
    // update cache
    machine.id = this.getLastId() + 1;

    this.machines$.getValue().push(machine);
    this.machines$.next(this.machines$.getValue());
    // });
  }

  deleteMachine(machineId: number) {
    // return this.http.post<MachineI>(environment.apiUrl + `/machines`).subscribe(result => {
    // update cache
    this.machines$.next(this.machines$.getValue().filter((machine: Machine) => machine.id !== machineId));
    // });
  }

  getWashingMachine(id: number): Observable<Machine> {
    return this.machines$.pipe(
      map((machines: Machine[]) => {
        return machines.find((machine: Machine) => machine.id === id);
      })
    );
  }

  getWashingMachinesByIds(ids: number[]): Observable<Machine[]> {
    return this.machines$.pipe(
      map((machines: Machine[]) => {
        return machines.filter((machine: Machine) => ids.indexOf(machine.id) !== -1);
      })
    );
  }

  getWashingMachines(): Observable<Machine[]> {
    return this.machines$;
  }

  // helper method to get highest id, so it can increment for the next machine
  // This should happen on the backend
  private getLastId() {
    const ids = this.machines$.getValue().map(machine => machine.id);
    console.log(ids);
    return ids[ids.length - 1];
  }
}
