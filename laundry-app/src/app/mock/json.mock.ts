export let LAUNDRY_ROOMS = [
  {
    id: 1,
    name: 'Main Hall',
    machines: [1, 2, 3],
    location: 'Main Street 21, NY, 300235',
    state: 'OPEN'
  },
  {
    id: 2,
    name: 'Silk Touch',
    machines: [4,6],
    location: 'Main Street 21, NY, 300235',
    state: 'OPEN'
  },
  {
    id: 3,
    name: 'Only White Room',
    machines: [5],
    location: 'Main Street 21, NY, 300235',
    state: 'OPEN'
  }
];

export let WASHING_MACHINES = [
  {
    id: 1,
    name: 'Standard 1',
    technicalName: 'WDB 020 Eco',
    imageUrl: 'https://media.miele.com/images/2000013/200001325/20000132578.png',
    sn: 'FDG234X',
    state: 'ACTIVE',
    door: 'LOCKED',
    activeSession: {
      temperature: '30',
      rotations: '1000',
      timeLeft: ''
    }
  },
  {
    id: 2,
    name: 'Standard 2',
    technicalName: 'WDB 020 Eco',
    imageUrl: 'https://media.miele.com/images/2000013/200001325/20000132578.png',
    sn: 'FDG234X',
    state: 'ACTIVE',
    door: 'LOCKED',
    activeSession: {
      temperature: '60',
      rotations: '1200',
      timeLeft: ''
    }
  },
  {
    id: 3,
    name: 'Standard 3',
    technicalName: 'WDB 020 Eco',
    imageUrl: 'https://media.miele.com/images/2000013/200001325/20000132578.png',
    sn: 'FDG234X',
    state: 'ACTIVE',
    door: 'LOCKED',
    activeSession: {
      temperature: '60',
      rotations: '800',
      timeLeft: ''
    }
  },
  {
    id: 4,
    name: 'WWD 320 WCS',
    technicalName: 'WWD 320 WCS PWash',
    imageUrl: 'https://media.miele.com/images/2000016/200001634/20000163428.png',
    sn: 'FDG234X',
    state: 'STANDBY',
    door: 'CLOSED',
    activeSession: null
  },
  {
    id: 5,
    name: 'WWG 360 WCS',
    technicalName: 'WWG 360 WCS PWash & 9kg',
    imageUrl: 'https://media.miele.com/images/2000016/200001634/20000163430.png',
    sn: 'FDG234C',
    state: 'OFF',
    door: 'OPEN',
    activeSession: null
  },
  {
    id: 6,
    name: 'WER 865 WPS',
    technicalName: 'WER 865 WPS PWash & TDos & 9kg',
    imageUrl: 'https://media.miele.com/images/2000016/200001618/20000161829.png',
    sn: 'FDG234C',
    state: 'OFF',
    door: 'OPEN',
    activeSession: null
  }
];

