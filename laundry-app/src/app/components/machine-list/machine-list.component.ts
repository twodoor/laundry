import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { MachinesService } from 'src/app/services/machines.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Machine } from 'src/app/models/machine';

@Component({
  selector: 'app-machine-list',
  templateUrl: './machine-list.component.html',
  styleUrls: ['./machine-list.component.scss']
})
export class MachineListComponent implements OnInit, OnChanges {
  @Input() machineIds: number[];
  public machines: Observable<Machine[]>;

  constructor(private machinesService: MachinesService, private router: Router) { }

  ngOnInit() {
    this.filterMachinesByIds(this.machineIds);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && changes.machineIds) {
      this.filterMachinesByIds(changes.machineIds.currentValue);
    }
  }

  getMachines(): Observable<Machine[]> {
    return this.machinesService.getWashingMachines();
  }

  editMachine(machine: Machine) {
    this.router.navigate(['washing-machines/edit', machine.id]);
  }

  deleteMachine(machine: Machine) {
    if (confirm(`Are you sure you want to delete ${machine.name}?`)) {
      this.machinesService.deleteMachine(machine.id);
    }
  }

  private filterMachinesByIds(machineIds: number[]) {
    this.machines = machineIds ?
      this.machinesService.getWashingMachinesByIds(machineIds) :
      this.machinesService.getWashingMachines();
  }
}
