import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineListComponent } from './machine-list.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from '../header/header.component';
import { RoomsMenuComponent } from '../rooms-menu/rooms-menu.component';
import { MachinesMenuComponent } from '../machines-menu/machines-menu.component';
import { MachineCardComponent } from '../machine-card/machine-card.component';
import { StateToIconPipe } from 'src/app/pipes/state-to-icon.pipe';

describe('MachineListComponent', () => {
  let component: MachineListComponent;
  let fixture: ComponentFixture<MachineListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule
      ],
      declarations: [
        HeaderComponent,
        RoomsMenuComponent,
        MachinesMenuComponent,
        MachineCardComponent,
        MachineListComponent,
        StateToIconPipe
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
