import { Component, OnInit, Input } from '@angular/core';
import { MachinesService } from 'src/app/services/machines.service';
import { Machine } from 'src/app/models/machine';
import { Router } from '@angular/router';

@Component({
  selector: 'app-machines-menu',
  templateUrl: './machines-menu.component.html',
  styleUrls: ['./machines-menu.component.scss']
})
export class MachinesMenuComponent implements OnInit {

  @Input() machines: Machine[];

  constructor(private machinesService: MachinesService, private router: Router) { }

  ngOnInit() {
    this.machinesService.getWashingMachines().subscribe((machines: Machine[]) => {
      this.machines = machines;
    });
  }

  navigateToMachines() {
    this.router.navigate(['washing-machines']);
  }
}
