import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachinesMenuComponent } from './machines-menu.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from '../header/header.component';
import { RoomsMenuComponent } from '../rooms-menu/rooms-menu.component';
import { MachineCardComponent } from '../machine-card/machine-card.component';
import { StateToIconPipe } from 'src/app/pipes/state-to-icon.pipe';

describe('MachinesMenuComponent', () => {
  let component: MachinesMenuComponent;
  let fixture: ComponentFixture<MachinesMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule
      ],
      declarations: [
        HeaderComponent,
        RoomsMenuComponent,
        MachinesMenuComponent,
        MachineCardComponent,
        StateToIconPipe
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachinesMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
