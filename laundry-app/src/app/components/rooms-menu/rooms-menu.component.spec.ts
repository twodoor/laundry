import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomsMenuComponent } from './rooms-menu.component';
import { MachineCardComponent } from '../machine-card/machine-card.component';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { StateToIconPipe } from 'src/app/pipes/state-to-icon.pipe';

describe('RoomsMenuComponent', () => {
  let component: RoomsMenuComponent;
  let fixture: ComponentFixture<RoomsMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [RoomsMenuComponent, MachineCardComponent, StateToIconPipe],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomsMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
