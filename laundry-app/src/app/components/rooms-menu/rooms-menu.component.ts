import { Component, OnInit } from '@angular/core';
import { RoomsService } from '../../services/rooms.service';
import { Room } from '../../models/room';
import { MachinesService } from 'src/app/services/machines.service';

@Component({
  selector: 'app-rooms-menu',
  templateUrl: './rooms-menu.component.html',
  styleUrls: ['./rooms-menu.component.scss']
})
export class RoomsMenuComponent implements OnInit {

  laundryRooms: Room[];
  activeRoom: Room;

  constructor(private roomService: RoomsService, private machinesService: MachinesService) { }

  ngOnInit() {
    this.roomService.getRooms().subscribe((rooms: Room[]) => {
      this.laundryRooms = rooms;
    });
  }

  getMachine(id: number) {
    return this.machinesService.getWashingMachine(id);
  }

  onRoomMouseOver(room: Room) {
    this.activeRoom = room;
  }
}
