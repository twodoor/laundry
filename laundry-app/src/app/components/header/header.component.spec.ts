import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderComponent } from './header.component';
import { RouterTestingModule } from '@angular/router/testing';
import { RoomsMenuComponent } from '../rooms-menu/rooms-menu.component';
import { MachinesMenuComponent } from '../machines-menu/machines-menu.component';
import { MachineCardComponent } from '../machine-card/machine-card.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StateToIconPipe } from 'src/app/pipes/state-to-icon.pipe';

fdescribe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule
      ],
      declarations: [
        HeaderComponent,
        RoomsMenuComponent,
        MachinesMenuComponent,
        MachineCardComponent,
        StateToIconPipe
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
