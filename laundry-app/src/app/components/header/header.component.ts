import { Component, OnInit, HostListener } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  group,
  query,
  animateChild,
} from '@angular/animations';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [
    // Header Animation , handles the opacity of the header
    trigger('headerAnimation', [
      state('show', style({
        background: 'rgb(31, 31, 31)'
      })),
      state('hide', style({
        background: 'rgba(31, 31, 31, 0.25)'
      })),
      transition('show => hide',
        group([
          query('@menuAnimation', [animateChild()]),
          animate('600ms ease-out')
        ])
      ),
      transition('hide => show',
        group([
          query('@menuAnimation', [animateChild()]),
          animate('600ms ease-in')
        ]))
    ]),

    // Menu Animation, handles the expantion/collaption of the nav menu
    trigger('menuAnimation', [
      state('show', style({
        opacity: 1,
        visibility: 'visible',
        height: '60px',
      })),
      state('hide', style({
        opacity: 0,
        visibility: 'hidden',
        height: 0,
      })),
      transition('show => hide', animate('300ms')),
      transition('hide => show', animate('300ms'))
    ])
  ]
})
export class HeaderComponent implements OnInit {

  menuState = 'show';
  showOverlay = false;
  activeMenu: string;

  public manuItems = [
    {
      label: 'Laundry Rooms',
      path: '/laundry-rooms/',
      name: 'laundry'
    },
    {
      label: 'Washing Machines',
      path: '/washing-machines/',
      name: 'machines'
    }
  ];

  @HostListener('window:scroll', ['$event']) // for window scroll events
  onWindowScroll(event) {
    const verticalOffset = window.pageYOffset
      || document.documentElement.scrollTop
      || document.body.scrollTop || 0;

    this.menuState = verticalOffset > 1 ? 'hide' : 'show';
    this.activeMenu = '';
  }

  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
    this.router.events.subscribe(routerObj => {
      this.activeMenu = '';
    });
  }

  onMachinesMenuClicked() {
    this.router.navigate(['/washing-machines']);
  }

  onMenuContainerMouseOver(event: MouseEvent) {
  }

  onMenuItemMouseOver(event: MouseEvent, activeMenu = '') {
    this.activeMenu = activeMenu;
  }

  onMenuMouseLeave(event: MouseEvent) {
    this.activeMenu = '';
  }
}
