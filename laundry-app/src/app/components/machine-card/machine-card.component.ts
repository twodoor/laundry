import { Component, OnInit, Input } from '@angular/core';
import { Machine } from 'src/app/models/machine';
import { MachinesService } from 'src/app/services/machines.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-machine-card',
  templateUrl: './machine-card.component.html',
  styleUrls: ['./machine-card.component.scss']
})
export class MachineCardComponent implements OnInit {
  @Input() machineId: number;

  machine: Machine = new Machine({});
  constructor(private machinesService: MachinesService, private router: Router) { }

  ngOnInit() {
    this.machinesService.getWashingMachine(this.machineId).subscribe((machine: Machine) => {
      this.machine = machine;
    });
  }

  onMachineCardClicked(machine: Machine) {
    this.router.navigate(['washing-machines/edit', machine.id]);
  }
}
