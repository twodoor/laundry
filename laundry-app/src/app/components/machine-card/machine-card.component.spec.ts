import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MachineCardComponent } from './machine-card.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from '../header/header.component';
import { RoomsMenuComponent } from '../rooms-menu/rooms-menu.component';
import { MachinesMenuComponent } from '../machines-menu/machines-menu.component';
import { StateToIconPipe } from 'src/app/pipes/state-to-icon.pipe';

describe('MachineCardComponent', () => {
  let component: MachineCardComponent;
  let fixture: ComponentFixture<MachineCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule
      ],
      declarations: [
        HeaderComponent,
        RoomsMenuComponent,
        MachinesMenuComponent,
        MachineCardComponent,
        StateToIconPipe
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MachineCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
