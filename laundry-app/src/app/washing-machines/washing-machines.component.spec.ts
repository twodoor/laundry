import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WashingMachinesComponent } from './washing-machines.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from '../app.component';
import { HeaderComponent } from '../components/header/header.component';
import { RoomsMenuComponent } from '../components/rooms-menu/rooms-menu.component';
import { MachinesMenuComponent } from '../components/machines-menu/machines-menu.component';
import { MachineCardComponent } from '../components/machine-card/machine-card.component';
import { MachineListComponent } from '../components/machine-list/machine-list.component';
import { StateToIconPipe } from '../pipes/state-to-icon.pipe';

describe('WashingMachinesComponent', () => {
  let component: WashingMachinesComponent;
  let fixture: ComponentFixture<WashingMachinesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent,
        HeaderComponent,
        RoomsMenuComponent,
        MachinesMenuComponent,
        MachineCardComponent,
        MachineListComponent,
        WashingMachinesComponent,
        StateToIconPipe
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WashingMachinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
