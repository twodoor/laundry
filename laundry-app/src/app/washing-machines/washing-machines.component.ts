import { Component, OnInit } from '@angular/core';
import { MachinesService } from '../services/machines.service';
import { Observable } from 'rxjs';
import { Machine } from '../models/machine';
import { Router } from '@angular/router';

@Component({
  selector: 'app-washing-machines',
  templateUrl: './washing-machines.component.html',
  styleUrls: ['./washing-machines.component.scss']
})
export class WashingMachinesComponent implements OnInit {
  constructor() { }

  ngOnInit() {
  }
}
