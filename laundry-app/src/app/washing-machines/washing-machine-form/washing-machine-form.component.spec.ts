import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WashingMachineFormComponent } from './washing-machine-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from 'src/app/app.component';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { RoomsMenuComponent } from 'src/app/components/rooms-menu/rooms-menu.component';
import { MachinesMenuComponent } from 'src/app/components/machines-menu/machines-menu.component';
import { MachineCardComponent } from 'src/app/components/machine-card/machine-card.component';
import { MachineListComponent } from 'src/app/components/machine-list/machine-list.component';
import { Machine } from 'src/app/models/machine';
import { MachinesService } from 'src/app/services/machines.service';
import { WashingMachinesComponent } from '../washing-machines.component';
import { StateToIconPipe } from 'src/app/pipes/state-to-icon.pipe';

describe('WashingMachineFormComponent', () => {
  let component: WashingMachineFormComponent;
  let fixture: ComponentFixture<WashingMachineFormComponent>;
  let machinesService: MachinesService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([{ path: 'washing-machines', component: WashingMachinesComponent }]),
        ReactiveFormsModule
      ],
      declarations: [
        AppComponent,
        HeaderComponent,
        RoomsMenuComponent,
        MachinesMenuComponent,
        MachineCardComponent,
        WashingMachineFormComponent,
        MachineListComponent,
        WashingMachinesComponent,
        StateToIconPipe
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WashingMachineFormComponent);
    component = fixture.componentInstance;
    machinesService = fixture.debugElement.injector.get(MachinesService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create a new washing machine if ID = 0', () => {
    const mockMachine = new Machine({ name: 'testName', id: 0 });
    component.machine = mockMachine;
    const machineCreateServiceSpy = spyOn(machinesService, 'createMachine');
    component.onSubmit();
    expect(machineCreateServiceSpy).toHaveBeenCalled();
  });

  it('should update a existing washing machine if ID != 0', () => {
    const mockMachine = new Machine({ name: 'testName', id: 3 });
    component.machine = mockMachine;
    const updateMachineServiceSpy = spyOn(machinesService, 'updateMachine');
    component.onSubmit();
    expect(updateMachineServiceSpy).toHaveBeenCalled();
  });


});
