import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MachinesService } from 'src/app/services/machines.service';
import { Machine } from 'src/app/models/machine';

@Component({
  selector: 'app-washing-machine-form',
  templateUrl: './washing-machine-form.component.html',
  styleUrls: ['./washing-machine-form.component.scss']
})
export class WashingMachineFormComponent implements OnInit {
  public machineForm: FormGroup;
  public machine: Machine = new Machine({});

  name = new FormControl();
  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private machinesService: MachinesService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.machineForm = this.formBuilder.group({
      name: ['', Validators.required],
      sn: ['', Validators.required],
      technicalName: '',
      state: '',
      door: [{ value: '', disabled: true }]
    });

    this.activatedRoute.paramMap.subscribe(params => {
      if (!params.get('id')) {
        return;
      }
      const machineId = parseInt(params.get('id'), 10);
      this.updateFormData(machineId);
    });
  }

  onSubmit() {
    this.machine = { ...this.machine, ...this.machineForm.value };
    if (this.machine.id > 0) {
      this.machinesService.updateMachine(this.machine.id, this.machine);
    } else {
      this.machinesService.createMachine(this.machine);
    }
    this.navigateToMachines();
  }

  onCancelClicked() {
    this.navigateToMachines();
  }

  processFile(imageInput: any) {
    const file: File = imageInput.files[0];
    const reader = new FileReader();

    reader.addEventListener('load', (event: any) => {
      this.machine.imageUrl = event.target.result;
    });

    reader.readAsDataURL(file);
  }

  private navigateToMachines() {
    this.router.navigate(['washing-machines']);
  }

  private updateFormData(machineId: number) {
    this.machinesService.getWashingMachine(machineId).subscribe((machine: Machine) => {
      if (machine) {
        this.machineForm.patchValue(machine);
        this.machine = new Machine(machine);
      }
    });
  }

}

