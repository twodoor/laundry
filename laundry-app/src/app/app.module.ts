import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './home/home.component';
import { RoomsMenuComponent } from './components/rooms-menu/rooms-menu.component';
import { RoomsService } from './services/rooms.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { MachineCardComponent } from './components/machine-card/machine-card.component';
import { MachinesMenuComponent } from './components/machines-menu/machines-menu.component';
import { WashingMachinesComponent } from './washing-machines/washing-machines.component';
import { WashingMachineFormComponent } from './washing-machines/washing-machine-form/washing-machine-form.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MachineListComponent } from './components/machine-list/machine-list.component';
import { LaundryRoomsComponent } from './laundry-rooms/laundry-rooms.component';
import { StateToIconPipe } from './pipes/state-to-icon.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    RoomsMenuComponent,
    MachineCardComponent,
    MachinesMenuComponent,
    WashingMachinesComponent,
    WashingMachineFormComponent,
    MachineListComponent,
    LaundryRoomsComponent,
    StateToIconPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [RoomsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
