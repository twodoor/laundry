import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Room } from '../models/room';
import { RoomsService } from '../services/rooms.service';

@Component({
  selector: 'app-laundry-rooms',
  templateUrl: './laundry-rooms.component.html',
  styleUrls: ['./laundry-rooms.component.scss']
})
export class LaundryRoomsComponent implements OnInit {
  public machineIds: number[];
  public room: Room;

  constructor(
    private activatedRoute: ActivatedRoute,
    private roomsService: RoomsService
  ) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      if (!params.get('id')) {
        return;
      }

      const roomId = parseInt(params.get('id'), 10);
      this.roomsService.getLaundryRoom(roomId).subscribe((room: Room) => {
        this.room = room;
        this.machineIds = room.machines;
      });
      // console.log(machineId);
    });
  }

}
