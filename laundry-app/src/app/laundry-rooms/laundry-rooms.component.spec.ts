import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaundryRoomsComponent } from './laundry-rooms.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from '../app.component';
import { HeaderComponent } from '../components/header/header.component';
import { RoomsMenuComponent } from '../components/rooms-menu/rooms-menu.component';
import { MachinesMenuComponent } from '../components/machines-menu/machines-menu.component';
import { MachineCardComponent } from '../components/machine-card/machine-card.component';
import { MachineListComponent } from '../components/machine-list/machine-list.component';
import { StateToIconPipe } from '../pipes/state-to-icon.pipe';

describe('LaundryRoomsComponent', () => {
  let component: LaundryRoomsComponent;
  let fixture: ComponentFixture<LaundryRoomsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        HeaderComponent,
        RoomsMenuComponent,
        MachinesMenuComponent,
        MachineCardComponent,
        MachineListComponent,
        LaundryRoomsComponent,
        StateToIconPipe
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaundryRoomsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
