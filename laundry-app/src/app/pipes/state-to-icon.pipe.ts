import { Pipe, PipeTransform } from '@angular/core';
import { MachineState } from '../models/machine-state.enum';

@Pipe({
  name: 'stateToIcon'
})
export class StateToIconPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    switch (value) {
      case MachineState.active:
        return `<i class="fa fa-toggle-on" aria-hidden="true"></i>`;
      case MachineState.off:
        return `<i class="fa fa-power-off" aria-hidden="true"></i>`;
      case MachineState.offline:
        return "";
      case MachineState.standby:
        return `<i class="fa fa-clock-o" aria-hidden="true"></i>`;
    }
    return null;
  }

}
