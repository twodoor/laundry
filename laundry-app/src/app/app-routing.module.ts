import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { WashingMachinesComponent } from './washing-machines/washing-machines.component';
import { WashingMachineFormComponent } from './washing-machines/washing-machine-form/washing-machine-form.component';
import { LaundryRoomsComponent } from './laundry-rooms/laundry-rooms.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'washing-machines', component: WashingMachinesComponent},
  { path: 'washing-machines/new', component: WashingMachineFormComponent},
  { path: 'washing-machines/edit/:id', component: WashingMachineFormComponent},
  { path: 'laundry-rooms/', component: LaundryRoomsComponent},
  { path: 'laundry-rooms/:id', component: LaundryRoomsComponent},
  { path: '**', redirectTo: '/home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
