export enum MachineState {
  active = 'ACTIVE',
  standby = 'STANDBY',
  off = 'OFF',
  offline = 'OFFLINE'
}
