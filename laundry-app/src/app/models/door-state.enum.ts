export enum DoorState {
  open = 'OPEN',
  closed = 'CLOSED',
  locked = 'LOCKED'
}
