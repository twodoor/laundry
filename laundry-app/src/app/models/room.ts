import { RoomState } from './room-state.enum';


export class Room {
  id: number;
  name: string;
  machines: any[];
  location: string;
  state: RoomState;

  constructor(roomObject: RoomI) {
    const {
      id = 0,
      name = '',
      machines = [],
      location = '',
      state = RoomState.open
    } = roomObject;

    this.id = id;
    this.name = name;
    this.machines = machines;
    this.location = location;
    this.state = state;
  }
}

export interface RoomI {
  id: number;
  name: string;
  machines: any[];
  location: string;
  state: RoomState;
}
