import { MachineState } from './machine-state.enum';
import { DoorState } from './door-state.enum';

export class Machine {
  id: number;
  name: string;
  technicalName: string;
  imageUrl: string;
  sn: string;
  state: MachineState;
  door: DoorState;
  activeSession: Session;

  constructor(machineObject: MachineI) {
    const {
      id = 0,
      name = '',
      technicalName = '',
      imageUrl = '',
      sn = '',
      state = MachineState.offline,
      door = DoorState.closed,
      activeSession = null
    } = machineObject;

    this.id = id;
    this.name = name;
    this.technicalName = technicalName;
    this.imageUrl = imageUrl;
    this.sn = sn;
    this.state = state;
    this.door = door;
    this.activeSession = activeSession;
  }
}

export interface MachineI {
  id?: number;
  name?: string;
  technicalName?: string;
  imageUrl?: string;
  sn?: string;
  state?: MachineState;
  door?: DoorState;
  activeSession?: Session;
}

export class Session {
  temperature: number;
  rpm: number;
  timeleft: string;
}
